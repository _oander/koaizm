#!/bin/sh

if [ "$1" == "" ]; then
    echo "use: ./getter.sh key";
    exit;
fi

if [ -f .env ]
    then
        export $(cat .env | sed 's/#.*//g' | xargs)
fi

curl \
-H "Content-Type: text/plain" \
-H 'Accept: text/plain' \
-v "http://127.0.0.1:$KOA_API_PORT/$1"