#!/bin/sh

if [ "$1" == "" ] || [ "$2" == "" ]; then
    echo "use: ./setter.sh key value";
    exit;
fi

if [ -f .env ]
    then
        export $(cat .env | sed 's/#.*//g' | xargs)
fi

curl --data "$2" \
-H "Content-Type: text/plain" \
-H 'Accept: text/plain' \
-v "http://127.0.0.1:$KOA_API_PORT/$1"
