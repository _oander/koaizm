const request = require('supertest');
const server = require('../app/index.js');

const getRandomString = (_length) => {
    var _str = 'ABCDEFGHIJKLM1234567890abcdefghijklm'.split(''), _newStr = [];
    for (let i = 0; i < _length; ++i) {
        _newStr.push(
            _str.splice(Math.floor(Math.random() * _str.length), 1)
        );
    }
    return _newStr.join('');
};

const _randomKeyNotFound = getRandomString(16);
const _key = getRandomString(8);
const _value = getRandomString(20);

beforeAll(async () => {
    // console.log('test is starting');
});

afterAll(() => {
    server.close();
    // console.log('test is finished');
});

describe('http tests', () => {

    test('empty key - http GET /', async () => {
        const response = await request(server)
            .get('/');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid url as key - empty string');
    });

    test('wrong url - http GET /key1/key2/key3', async () => {
        const response = await request(server)
            .get('/key1/key2/key3');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid url as key');
    });

    test('empty key - http POST /', async () => {
        const response = await request(server)
            .post('/')
            .set('Content-type', 'text/plain')
            .send('valamimivanitten');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid url as key - empty string');
    });

    test('wrong url - http POST /key1/key2/key3', async () => {
        const response = await request(server)
            .post('/key1/key2/key3')
            .set('Content-type', 'text/plain')
            .send('valamimivanitten');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid url as key');
    });

    test('empty body - http POST /somekeytosave', async () => {
        const response = await request(server)
            .post('/somekeytosave')
            .set('Content-type', 'text/plain')
            .send('');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid or missing body for value');
    });

    test(`key not found - http GET /${_randomKeyNotFound}`, async () => {
        const response = await request(server)
        .get(`/${_randomKeyNotFound}`);
        expect(response.status).toEqual(404);
        expect(response.text).toContain('key not found');
    });

    test('invalid content type - http POST /somekeytosave', async () => {
        const response = await request(server)
            .post('/somekeytosave')
            .set('Content-type', 'application/json')
            .send('somevaluetosave');
        expect(response.status).toEqual(500);
        expect(response.text).toContain('invalid content type');
    });

    test(`successfull insert - http POST /${_key}`, async () => {
        const response = await request(server)
            .post(`/${_key}`)
            .set('Content-type', 'text/plain')
            .send(`${_value}`);
        expect(response.status).toEqual(200);
        expect(response.text).toContain(`Successfully insert: key: ${_key}, value: ${_value}`);
    });

    test(`successfull get - http GET /${_key}`, async () => {
        const response = await request(server)
        .get(`/${_key}`);
        expect(response.status).toEqual(200);
        expect(response.text).toContain(`${_value}`);
    });
});