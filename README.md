# koa api demo a test task for oander

## Installation  

#### setup env-example
#### cp env-example .env
#### run init.sh
#### docker-compose up -d

## Using
#### ./setter.sh somekey somevalue
#### ./getter.sh somekey

## Testing
#### get container id with docker ps
#### exec to the container
#### npm run test