/*--------------------------
  :: Interface Controller
---------------------------*/

const _redis = require('../proto/redisinterface');

function Controller() {

    var ctx = {}, redis = {}, __identity = '';
};

Controller.prototype._constructor = (_ctx, _identity) => {
    ctx = _ctx;
    redis = _redis;
    __identity = _identity;
};

Controller.prototype.identity = () => __identity;

Controller.prototype.index = () => {};

Controller.prototype.read = () => {};

Controller.prototype.create = () => {};

Controller.prototype.update = () => {};

Controller.prototype.delete = () => {};

module.exports = Controller;