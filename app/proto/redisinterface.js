/*-----------------------------
  :: Singleton RedisInterface
------------------------------*/

const Redis = require('ioredis');

function RedisInterface() {

    var _redis = {};

    const _constructor = () => {
        _redis = new Redis({
            host: 'redis',
            port: process.env.REDIS_PORT_NUMBER,
            password: process.env.REDIS_PASSWORD
        });
    };

    const get = (_key) => {
        return new Promise((resolve, reject) => _redis.get(_key, (_error, _result) => _error 
            && reject(_error)
            || resolve(_result)));
    };

    const set = (_key, _value) => {
        return new Promise((resolve, reject) => {
            try {
                _redis.set(_key, _value);
            } catch (_error) {
                reject(_error);
            }
            resolve(true);
        });
    };

    _constructor();

    return {
        get: get,
        set: set
    };
};

module.exports = new RedisInterface();