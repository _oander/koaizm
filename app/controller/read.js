/*------------------
  :: Read
-------------------*/
 
 const _controller = require('../proto/controller');

module.exports = (_ctx) => {

    _controller.prototype.read = function() {

        const _key = _ctx._api._key;

        return new Promise((resolve, reject) => redis.get(_key)
            .then(_result => {
                return (_result === null)
                    ? (
                        ctx.logger.warn('The key was not found: ' + _key),
                        reject({
                            code: 404,
                            message: 'key not found'
                        })
                    )
                    : (
                        ctx.logger.info('Successfull query: ' + _result),
                        resolve(_result)
                    );
            })
            .catch(_error => (
                ctx.logger.error(_error),
                reject({
                    code: 500,
                    message: 'Something went wrong'
                })
            )));
    };

    _controller.prototype._constructor(_ctx, 'Read');

    return new _controller();
};