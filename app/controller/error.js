/*------------------
  :: Error
-------------------*/
 
 const _controller = require('../proto/controller');

module.exports = (_ctx) => {

    _controller.prototype.index = function(_error) {

        return new Promise((resolve, reject) => {
            _ctx.response.status = _error.code;
            _ctx.response.message = _error.message;
            return resolve();
        });
    };

    _controller.prototype._constructor(_ctx, 'Error');

    return new _controller();
};