/*------------------
  :: Create
-------------------*/

 const _controller = require('../proto/controller');

 module.exports = (_ctx) => {

    _controller.prototype.create = function() {

        const _propz = _ctx._api;

        return new Promise((resolve, reject) => redis.set(_propz._key, _propz._value)
            .then(_result => (
                ctx.logger.info(`Successfully insert: key: ${_propz._key}, value: ${_propz._value}`),
                resolve(`Successfully insert: key: ${_propz._key}, value: ${_propz._value}`)
            ))
            .catch(_error => (
                ctx.logger.error(_error),
                reject({
                    code: 500,
                    message: 'Something went wrong'
                })
            )));
    };

    _controller.prototype._constructor(_ctx, 'Create');

    return new _controller();
};