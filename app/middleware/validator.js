/*-----------------------------------
  :: Singleton Middleware Validator
-----------------------------------*/

function Validator() {

    const scenarios = ['checkContentType', 'checkUrl', 'checkBody'];

    const _constructor = () => {};

    const checkContentType = (_ctx) => {

        if (_ctx.request.method === 'POST' && !_ctx.is('text/*', 'text/plain')) {
            throw 'invalid content type';
        }
    };

    const checkUrl = (_ctx) => {

        const parts = _ctx.request.url.split('/');
        const key = parts.join('');

        if (parts.length !== 2) {
            throw 'invalid url as key';
        }

        if (key.length === 0) {
            throw 'invalid url as key - empty string';
        }

        _ctx._api._key = key;
    };

    const checkBody = (_ctx) => {

        if (_ctx.request.method === 'POST' && _ctx.request.rawBody === '') {
            throw 'invalid or missing body for value';
        }

        _ctx._api._value = _ctx.request.rawBody;
    };

    const validate = async (_ctx, next) => {

        var error = '';

        _ctx._api = {};

        try {
            // i know: eval is evil. but not in this case.
            scenarios.forEach(_scenario => eval(_scenario)(_ctx));
        } catch (_error) {
            error = _error;
            _ctx.logger.error('Validator::validate: ' + error);
        }

        // preventing middleware pipeline
        if (error !== '') {
            return require('../controller/error')(_ctx).index({code: 500, message: error})
                .then(_result => ctx.logger.warn('ErrorController has been successfully finished'));
        }

        return await next();
    };

    _constructor();

    return {
        validate:validate
    };
};

module.exports = new Validator();