/**
 * Singleton Middleware Router
 */

function Router() {

    const _constructor = () => {};

    const getController = (_ctx) => {

        return new Promise((resolve, reject) => {
            return (_ctx.request.method === 'POST')
                ? resolve({
                    instance: require('../controller/create')(_ctx),
                    method: 'create'
                })
                : resolve({
                    instance: require('../controller/read')(_ctx),
                    method: 'read'
                });
        });
    };

    const route = async (ctx, next) => {

        ctx.logger.info('Router::route');

        return getController(ctx)
        
            .then(_controller => {

                ctx.logger.info('Controller has been initialised: ' + _controller.instance.identity());

                return _controller.instance[_controller.method](ctx)
                    .then(_result => (
                        ctx.logger.info('The Request has been successfully finished'),
                        ctx.body = _result,
                        next()
                    ))
                    .catch(_error => require('../controller/error')(ctx).index(_error)
                        .then(_result => (
                            ctx.logger.warn('ErrorController has been successfully finished'),
                            next()
                        )));
            });
    };

    _constructor();

    return {
        route:route
    };
};

module.exports = new Router();