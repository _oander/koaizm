/*------------------
  :: Application
-------------------*/

require('dotenv').config();

const port = (typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV === 'test' ? 81 : 80);
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const pino = require('pino');
const Logger = pino({
    transport: {
        target: 'pino-pretty',
        options: {
            colorize: true
        }
    }
});

const checkDotenv = () => {
    if (typeof process.env.KOA_API_PORT === 'undefined'
        || typeof process.env.REDIS_PASSWORD === 'undefined'
            || typeof process.env.REDIS_PORT_NUMBER === 'undefined') {
        Logger.error('dotenv error');
        process.exit(1);
    }
};

checkDotenv();

const app = new Koa();
const Validator = require('./middleware/validator');
const Router = require('./middleware/router');

app.use(bodyParser({
    enableTypes: ['text', 'text/plain']
}));

// logger is accessable for middlewares through the http stack of koa
app.use(async (ctx, next) => (
    ctx.logger = Logger,
    await next()
));

// validates http request
app.use(Validator.validate);

// resolves http request
app.use(Router.route);

module.exports = app.listen(port, () => {
    Logger.info(' _               _ _____          ');
    Logger.info('| | _____   __ _(_)__  /_ __ ___  ');
    Logger.info('| |/ / _ \\ / _` | | / /| \'_ ` _ \\ ');
    Logger.info('|   < (_) | (_| | |/ /_| | | | | |');
    Logger.info('|_|\\_\\___/ \\__,_|_/____|_| |_| |_|');
    Logger.info('');
    Logger.info('app is running on %d', process.env.KOA_API_PORT);
});