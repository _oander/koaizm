#!/bin/bash

sudo rm redis-data -R;
sudo rm node_modules -R;
rm package-lock.json;
docker rm $(docker ps -aq);

mkdir redis-data;
mkdir node_modules;
sudo chmod 7777 redis-data;
sudo chmod 7777 node_modules;